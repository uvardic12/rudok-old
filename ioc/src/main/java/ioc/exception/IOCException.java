package ioc.exception;

public abstract class IOCException extends RuntimeException {

    public IOCException(String message) {
        super(message);
    }

}
