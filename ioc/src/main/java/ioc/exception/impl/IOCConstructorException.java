package ioc.exception.impl;

import ioc.exception.IOCException;

public class IOCConstructorException extends IOCException {

    public IOCConstructorException(String message) {
        super(message);
    }

}
