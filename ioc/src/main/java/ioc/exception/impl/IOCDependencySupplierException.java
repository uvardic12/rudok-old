package ioc.exception.impl;

import ioc.exception.IOCException;

public class IOCDependencySupplierException extends IOCException {

    public IOCDependencySupplierException(String message) {
        super(message);
    }

}
