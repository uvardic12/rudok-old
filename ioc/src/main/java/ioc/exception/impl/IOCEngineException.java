package ioc.exception.impl;

import ioc.exception.IOCException;

public class IOCEngineException extends IOCException {

    public IOCEngineException(String message) {
        super(message);
    }

}
