package ioc.engine;

import ioc.engine.initialize.ClassLoader;
import ioc.engine.initialize.ComponentFilter;
import ioc.engine.initialize.ComponentMapper;
import ioc.engine.model.ComponentModel;
import ioc.engine.supplier.InstanceSupplier;
import ioc.exception.impl.IOCEngineException;

import java.util.LinkedList;
import java.util.Queue;

import static java.util.stream.Collectors.toCollection;

public class IOCEngine {

    private static final int DEPTH_LIMIT = 10000;

    private IOCEngine() {}

    public static void start(Class<?> reference) {
        Queue<ComponentModel> components = initializeComponents(reference);

        int depth = 0;
        while (!components.isEmpty()) {
            ComponentModel component = components.remove();

            if (component.isResolvable())
                component.resolve();
            else {
                components.add(component);
                depth++;
            }

            if (depth == DEPTH_LIMIT)
                throw new IOCEngineException(
                        String.format("Component: %s can't be resolved or a cyclical dependency is formed! Max depth reached!", component)
                );
        }
    }

    private static Queue<ComponentModel> initializeComponents(Class<?> reference) {
        return ClassLoader.loadClasses(reference)
                .stream()
                .filter(new ComponentFilter())
                .map(new ComponentMapper())
                .distinct()
                .collect(toCollection(LinkedList::new));
    }

    public static <T> T get(Class<T> aClass) {
        return InstanceSupplier.getInstanceFor(aClass);
    }

}
