package ioc.engine.supplier;

import ioc.exception.impl.IOCDependencySupplierException;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

public class InstanceSupplier {

    private static final Set<Object> instantiatedClasses = new HashSet<>();

    private InstanceSupplier() {}

    public static void addInstantiatedClass(Object instance) {
        if (instance == null)
            throw new IOCDependencySupplierException("Instantiated class can't be null!");
        instantiatedClasses.add(instance);
    }

    public static boolean isDependencyResolvable(Field dependency) {
        return instantiatedClasses
                .stream()
                .anyMatch(instance -> dependency.getType().isAssignableFrom(instance.getClass()));
    }

    public static Object[] getInstancesFor(Set<Field> dependencies) {
        return dependencies
                .stream()
                .map(InstanceSupplier::getInstanceFor)
                .toArray();
    }

    private static Object getInstanceFor(Field dependency) {
        return instantiatedClasses.stream()
                .filter(instance -> dependency.getType().isAssignableFrom(instance.getClass()))
                .findFirst()
                .orElseThrow(() -> new IOCDependencySupplierException(
                        String.format("Instance for dependency: %s wasn't found!", dependency.getName())
                ));
    }

    public static <T> T getInstanceFor(Class<T> aClass) {
        return instantiatedClasses.stream()
                .filter(instance -> instance.getClass().equals(aClass))
                .map(aClass::cast)
                .findFirst()
                .orElseThrow(() -> new IOCDependencySupplierException(
                        String.format("Instance for class: %s wasn't found!", aClass)
                ));
    }

}
