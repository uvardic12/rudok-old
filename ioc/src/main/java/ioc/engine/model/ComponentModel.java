package ioc.engine.model;

import ioc.engine.supplier.InstanceSupplier;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.Set;

public class ComponentModel {

    private final String name;

    private final Constructor<?> constructor;

    private final Method initializeMethod;

    private final Set<Field> dependencies;

    private ComponentModel(Builder builder) {
        this.name = builder.name;
        this.constructor = builder.constructor;
        this.initializeMethod = builder.initializeMethod;
        this.dependencies = builder.dependencies;
    }

    public String getName() {
        return name;
    }

    public boolean isResolvable() {
        return dependencies.stream().allMatch(InstanceSupplier::isDependencyResolvable);
    }

    public void resolve() {
        try {
            Object[] dependencyInstances = InstanceSupplier.getInstancesFor(dependencies);
            Object instance = constructor.newInstance(dependencyInstances);
            if (initializeMethod != null)
                initializeMethod.invoke(instance);
            InstanceSupplier.addInstantiatedClass(instance);
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
        }

    }

    @Override
    public String toString() {
        return "ComponentModel{" +
                "name='" + name + '\'' +
                ", constructor=" + constructor +
                ", dependencies=" + dependencies +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ComponentModel other)
            return Objects.equals(this.name, other.name);
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.name);
    }

    public static class Builder {

        private String name;

        private Constructor<?> constructor;

        private Method initializeMethod;

        private Set<Field> dependencies;

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder constructor(Constructor<?> constructor) {
            if (constructor == null)
                throw new IllegalArgumentException("Constructor can't be null!");
            this.constructor = constructor;
            this.constructor.setAccessible(true);
            return this;
        }

        public Builder initializeMethod(Method initializeMethod) {
            this.initializeMethod = initializeMethod;
            if (initializeMethod != null)
                this.initializeMethod.setAccessible(true);
            return this;
        }

        public Builder dependencies(Set<Field> dependencies) {
            if (dependencies == null)
                throw new IllegalArgumentException("Dependencies can't be null!");
            this.dependencies = dependencies;
            this.dependencies.forEach(dependency -> dependency.setAccessible(true));
            return this;
        }

        public ComponentModel build() {
            return new ComponentModel(this);
        }

    }

}
