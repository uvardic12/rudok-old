package ioc.engine.initialize;


import ioc.engine.annotation.Component;
import ioc.engine.annotation.Initialize;
import ioc.engine.model.ComponentModel;
import ioc.exception.impl.IOCConstructorException;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Set;
import java.util.function.Function;

import static java.util.stream.Collectors.toSet;

public class ComponentMapper implements Function<Class<?>, ComponentModel> {

    @Override
    public ComponentModel apply(Class<?> aClass) {
        return new ComponentModel.Builder()
                .name(aClass.getName())
                .constructor(findConstructor(aClass))
                .initializeMethod(findInitializeMethod(aClass))
                .dependencies(findDependencies(aClass))
                .build();
    }

    private Constructor<?> findConstructor(Class<?> aClass) {
        Constructor<?>[] declaredConstructors = aClass.getDeclaredConstructors();
        if (declaredConstructors.length > 1)
            throw new IOCConstructorException(
                    String.format("More then 1 constructor found for class: %s. IOC Engine only supports classes with 1 constructor", aClass.getName())
            );
        return declaredConstructors[0];
    }

    private Method findInitializeMethod(Class<?> aClass) {
        Method[] declaredMethods = aClass.getDeclaredMethods();
        return Arrays.stream(declaredMethods)
                .filter(method -> method.isAnnotationPresent(Initialize.class))
                .filter(method -> method.getParameterCount() == 0)
                .findFirst()
                .orElse(null);
    }

    private Set<Field> findDependencies(Class<?> aClass) {
        Constructor<?> constructor = findConstructor(aClass);
        return Arrays.stream(aClass.getDeclaredFields())
                .filter(field -> isDependency(constructor, field))
                .collect(toSet());
    }

    private boolean isDependency(Constructor<?> constructor, Field field) {
        boolean isValid = field.getType().isAnnotationPresent(Component.class);
        if (!isValid)
            throw new IOCConstructorException(
                    String.format("Constructor contains field: %s that is not a component!", field.getName())
            );

        return Arrays.stream(constructor.getParameterTypes())
                .anyMatch(parameter -> field.getType().equals(parameter));
    }

}
