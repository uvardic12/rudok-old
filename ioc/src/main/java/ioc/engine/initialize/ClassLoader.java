package ioc.engine.initialize;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toSet;

public class ClassLoader {

    private ClassLoader() {}

    public static Set<Class<?>> loadClasses(Class<?> rootClass) {
        String directoryPath = rootClass.getProtectionDomain().getCodeSource().getLocation().getFile();
        return isJar(directoryPath) ? loadClassesFromJar(directoryPath) : loadClassesFromDirectory(directoryPath);
    }

    private static boolean isJar(String directoryPath) {
        return !new File(directoryPath).isDirectory() && directoryPath.endsWith(".jar");
    }

    private static Set<Class<?>> loadClassesFromJar(String jarFilePath) {
        try {
            return new JarFile(new File(jarFilePath))
                    .stream()
                    .filter(ClassLoader::isClass)
                    .map(jarEntry -> getClass(jarEntry.getName()))
                    .collect(toSet());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static boolean isClass(JarEntry jarEntry) {
        return jarEntry.getName().endsWith(".class") && !jarEntry.getName().contains("module-info");
    }

    private static final Set<Class<?>> loadedClasses = new HashSet<>();

    private static Set<Class<?>> loadClassesFromDirectory(String directoryPath) {
        loadedClasses.clear();
        loadClassesFromDirectoryWorker(new File(directoryPath), "");
        return loadedClasses;
    }

    private static void loadClassesFromDirectoryWorker(File file, String packageName) {
        if (file.isDirectory()) {
            packageName += file.getName() + ".";
            for (File subDir : requireNonNull(file.listFiles()))
                loadClassesFromDirectoryWorker(subDir, packageName);
        } else if (isClass(file)) {
            String className = packageName + file.getName();
            loadedClasses.add(getClass(className));
        }
    }

    private static boolean isClass(File file) {
        return file.getName().endsWith(".class") && !file.getName().contains("module-info");
    }

    private static Class<?> getClass(String className) {
        try {
            String formattedClassName = className
                    .replace("classes.", "")
                    .replace(".class", "")
                    .replaceAll("\\\\", ".")
                    .replaceAll("/", ".");
            return Class.forName(formattedClassName);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
