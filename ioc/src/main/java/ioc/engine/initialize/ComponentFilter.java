package ioc.engine.initialize;

import ioc.engine.annotation.Component;

import java.util.Arrays;
import java.util.function.Predicate;

public class ComponentFilter implements Predicate<Class<?>> {

    @Override
    public boolean test(Class<?> aClass) {
        return !aClass.isInterface() &&
                !aClass.isAnnotation() &&
                !aClass.isEnum() &&
                isComponent(aClass);
    }

    private boolean isComponent(Class<?> aClass) {
        return Arrays.stream(aClass.getDeclaredAnnotations())
                .anyMatch(annotation -> Component.class.equals(annotation.annotationType()));
    }

}
