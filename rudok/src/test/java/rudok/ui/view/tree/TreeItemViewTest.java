package rudok.ui.view.tree;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import rudok.model.workspace.Workspace;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TreeItemViewTest {

    private Workspace model;

    private TreeItemView treeItemView;

    @BeforeEach
    public void init() {
        model = Workspace.getInstance();
        treeItemView = new TreeItemView(model);
    }

    @Test
    public void getModelTest() {
        assertEquals(model, treeItemView.getModel());
    }

    @Test
    public void toStringTest() {
        assertEquals("Workspace", treeItemView.toString());
    }

    @Test
    public void equalsTest() {
        assertEquals(treeItemView, new TreeItemView(model));
    }

}
