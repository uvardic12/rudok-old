package rudok.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IdGeneratorTest {

//    @Test
    public void generateIdTest() {
        assertEquals("0", IdGenerator.generateId());
        assertEquals("1", IdGenerator.generateId());
        assertEquals("2", IdGenerator.generateId());
    }

}
