package rudok.model.workspace;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import rudok.model.MockVisitor;
import rudok.model.project.Project;
import rudok.observer.MockObserver;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class WorkspaceTest {

    private final Workspace workspace = new Workspace("Workspace");

    private final Project project = new Project("Child", workspace);

    private final ByteArrayOutputStream consoleStream = new ByteArrayOutputStream();

    private final PrintStream originalConsoleStream = System.out;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(consoleStream));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalConsoleStream);
    }

    @Test
    public void getInstanceTest() {
        assertNotNull(Workspace.getInstance());
        assertEquals(Workspace.getInstance(), Workspace.getInstance());
    }

    @Test
    public void getIdTest() {
        assertNotNull(workspace.getId());
        assertFalse(workspace.getId().isBlank());
    }

    @Test
    public void getNameTest() {
        assertEquals("Workspace", workspace.getName());
    }

    @Test
    public void setNameTest() {
        assertThrows(
                IllegalArgumentException.class,
                () -> workspace.setName(""),
                "Workspace name can't be blank!"
        );
        workspace.setName("Test");
        assertEquals("Test", workspace.getName());
    }

    @Test
    public void addChildTest() {
        assertTrue(workspace.getChildren().contains(project));
        assertThrows(
                IllegalArgumentException.class,
                () -> workspace.addChild(null),
                "Child can't be null!"
        );
        assertThrows(
                IllegalArgumentException.class,
                () -> {
                    workspace.addChild(project);
                    workspace.addChild(project);
                },
                String.format("Child %s is already present!", project)
        );
    }

    @Test
    public void removeChildTest() {
        assertTrue(workspace.getChildren().contains(project));
        workspace.removeChild(project);
        assertTrue(workspace.getChildren().isEmpty());
    }

    @Test
    public void getChildrenTest() {
        assertEquals(List.of(project), workspace.getChildren());
        assertThrows(
                UnsupportedOperationException.class,
                () -> workspace.getChildren().add(project)
        );
    }

    private final MockObserver mockObserver = new MockObserver();

    @Test
    public void addObserverTest() {
        workspace.addObserver(mockObserver);
        assertEquals(List.of(mockObserver), workspace.getObservers());
        assertThrows(
                IllegalArgumentException.class,
                () -> workspace.addObserver(null),
                "Observer can't be null!"
        );
        assertThrows(
                IllegalArgumentException.class,
                () -> {
                    workspace.addObserver(mockObserver);
                    workspace.addObserver(mockObserver);
                },
                String.format("Observer %s is already present!", mockObserver)
        );
    }

    @Test
    public void notifyAllObserversTest() {
        workspace.addObserver(mockObserver);
        workspace.notifyAllObservers();
        assertEquals(String.format("MockObserver-%s", workspace), consoleStream.toString().trim());
    }

    private final MockVisitor mockVisitor = new MockVisitor();

    @Test
    public void acceptVisitorTest() {
        workspace.acceptVisitor(mockVisitor);
        assertEquals(String.format("Visited %s", workspace), consoleStream.toString().trim());
    }

    @Test
    public void equalsTest() {
        assertEquals(workspace, workspace);
        assertNotEquals(workspace, new Workspace("Workspace"));
    }

}
