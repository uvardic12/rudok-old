package rudok.model.document;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import rudok.model.MockVisitor;
import rudok.model.page.Page;
import rudok.model.project.Project;
import rudok.model.workspace.Workspace;
import rudok.observer.MockObserver;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class DocumentTest {

    private final Workspace workspace = Workspace.getInstance();

    private final Project project = new Project("Parent", workspace);

    private final Document document = new Document("Document", project);

    private final Page page = new Page("Child", document);

    private final ByteArrayOutputStream consoleStream = new ByteArrayOutputStream();

    private final PrintStream originalConsoleStream = System.out;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(consoleStream));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalConsoleStream);
    }

    @Test
    public void blankInitTest() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new Document("", null),
                "Document name can't be blank!"
        );
    }

    @Test
    public void getIdTest() {
        assertNotNull(document.getId());
        assertFalse(document.getId().isBlank());
    }

    @Test
    public void getNameTest() {
        assertEquals("Document", document.getName());
    }

    @Test
    public void setNameTest() {
        assertThrows(
                IllegalArgumentException.class,
                () -> document.setName(""),
                "Document name can't be blank!"
        );
        document.setName("Test");
        assertEquals("Test", document.getName());
    }

    @Test
    public void addChildTest() {
        assertTrue(document.getChildren().contains(page));
        assertThrows(
                IllegalArgumentException.class,
                () -> document.addChild(null),
                "Child can't be null!"
        );
        assertThrows(
                IllegalArgumentException.class,
                () -> {
                    document.addChild(page);
                    document.addChild(page);
                },
                String.format("Child %s is already present!", page)
        );
    }

    @Test
    public void removeChildTest() {
        assertTrue(document.getChildren().contains(page));
        document.removeChild(page);
        assertTrue(document.getChildren().isEmpty());
    }

    @Test
    public void getChildrenTest() {
        assertEquals(List.of(page), document.getChildren());
        assertThrows(
                UnsupportedOperationException.class,
                () -> document.getChildren().add(page)
        );
    }

    @Test
    public void getParentTest() {
        assertEquals(project, document.getParent());
    }

    private final MockObserver mockObserver = new MockObserver();

    @Test
    public void addObserverTest() {
        document.addObserver(mockObserver);
        assertEquals(List.of(mockObserver), document.getObservers());
        assertThrows(
                IllegalArgumentException.class,
                () -> document.addObserver(null),
                "Observer can't be null!"
        );
        assertThrows(
                IllegalArgumentException.class,
                () -> {
                    document.addObserver(mockObserver);
                    document.addObserver(mockObserver);
                },
                String.format("Observer %s is already present!", mockObserver)
        );
    }

    @Test
    public void notifyAllObserversTest() {
        document.addObserver(mockObserver);
        document.notifyAllObservers();
        assertEquals(String.format("MockObserver-%s", document), consoleStream.toString().trim());
    }

    private final MockVisitor mockVisitor = new MockVisitor();

    @Test
    public void acceptVisitorTest() {
        document.acceptVisitor(mockVisitor);
        assertEquals(String.format("Visited %s", document), consoleStream.toString().trim());
    }

    @Test
    public void equalsTest() {
        assertEquals(document, document);
        assertNotEquals(document, new Document("Document", project));
    }

}
