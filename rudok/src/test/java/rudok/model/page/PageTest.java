package rudok.model.page;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import rudok.model.MockVisitor;
import rudok.model.document.Document;
import rudok.model.project.Project;
import rudok.model.slot.Slot;
import rudok.model.workspace.Workspace;
import rudok.observer.MockObserver;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PageTest {

    private final Workspace workspace = Workspace.getInstance();

    private final Project project = new Project("Project", workspace);

    private final Document document = new Document("Parent", project);

    private final Page page = new Page("Page", document);

    private final Slot slot = new Slot("Child", page);

    private final ByteArrayOutputStream consoleStream = new ByteArrayOutputStream();

    private final PrintStream originalConsoleStream = System.out;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(consoleStream));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalConsoleStream);
    }

    @Test
    public void blankInitTest() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new Page("", null),
                "Page name can't be blank!"
        );
    }

    @Test
    public void getIdTest() {
        assertNotNull(page.getId());
        assertFalse(page.getId().isBlank());
    }

    @Test
    public void getNameTest() {
        assertEquals("Page", page.getName());
    }

    @Test
    public void setNameTest() {
        assertThrows(
                IllegalArgumentException.class,
                () -> page.setName(""),
                "Page name can't be blank!"
        );
        page.setName("Test");
        assertEquals("Test", page.getName());
    }

    @Test
    public void addChildTest() {
        assertTrue(page.getChildren().contains(slot));
        assertThrows(
                IllegalArgumentException.class,
                () -> page.addChild(null),
                "Child can't be null!"
        );
        assertThrows(
                IllegalArgumentException.class,
                () -> {
                    page.addChild(slot);
                    page.addChild(slot);
                },
                String.format("Child %s is already present!", slot)
        );
    }

    @Test
    public void removeChildTest() {
        assertTrue(page.getChildren().contains(slot));
        page.removeChild(slot);
        assertTrue(page.getChildren().isEmpty());
    }

    @Test
    public void getChildrenTest() {
        assertEquals(List.of(slot), page.getChildren());
        assertThrows(
                UnsupportedOperationException.class,
                () -> page.getChildren().add(slot)
        );
    }

    @Test
    public void getParentTest() {
        assertEquals(document, page.getParent());
    }

    private final MockObserver mockObserver = new MockObserver();

    @Test
    public void addObserverTest() {
        page.addObserver(mockObserver);
        assertEquals(List.of(mockObserver), page.getObservers());
        assertThrows(
                IllegalArgumentException.class,
                () -> page.addObserver(null),
                "Observer can't be null!"
        );
        assertThrows(
                IllegalArgumentException.class,
                () -> {
                    page.addObserver(mockObserver);
                    page.addObserver(mockObserver);
                },
                String.format("Observer %s is already present!", mockObserver)
        );
    }

    @Test
    public void notifyAllObserversTest() {
        page.addObserver(mockObserver);
        page.notifyAllObservers();
        assertEquals(String.format("MockObserver-%s", page), consoleStream.toString().trim());
    }

    private final MockVisitor mockVisitor = new MockVisitor();

    @Test
    public void acceptVisitorTest() {
        page.acceptVisitor(mockVisitor);
        assertEquals(String.format("Visited %s", page), consoleStream.toString().trim());
    }

    @Test
    public void equalsTest() {
        assertEquals(page, page);
        assertNotEquals(page, new Page("Page", document));
    }

}
