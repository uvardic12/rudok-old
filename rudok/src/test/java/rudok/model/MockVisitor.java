package rudok.model;

import rudok.model.document.Document;
import rudok.model.page.Page;
import rudok.model.project.Project;
import rudok.model.slot.Slot;
import rudok.model.workspace.Workspace;

public class MockVisitor implements ModelVisitor {

    @Override
    public void visit(Workspace workspace) {
        System.out.printf("Visited %s%n", workspace);
    }

    @Override
    public void visit(Project project) {
        System.out.printf("Visited %s%n", project);
    }

    @Override
    public void visit(Document document) {
        System.out.printf("Visited %s%n", document);
    }

    @Override
    public void visit(Page page) {
        System.out.printf("Visited %s%n", page);
    }

    @Override
    public void visit(Slot slot) {
        System.out.printf("Visited %s%n", slot);
    }

}
