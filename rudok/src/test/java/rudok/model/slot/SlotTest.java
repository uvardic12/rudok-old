package rudok.model.slot;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import rudok.model.MockVisitor;
import rudok.model.document.Document;
import rudok.model.page.Page;
import rudok.model.project.Project;
import rudok.model.workspace.Workspace;
import rudok.observer.MockObserver;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class SlotTest {

    private final Workspace workspace = Workspace.getInstance();

    private final Project project = new Project("Project", workspace);

    private final Document document = new Document("Document", project);

    private final Page page = new Page("Parent", document);

    private final Slot slot = new Slot("Slot", page);

    private final ByteArrayOutputStream consoleStream = new ByteArrayOutputStream();

    private final PrintStream originalConsoleStream = System.out;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(consoleStream));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalConsoleStream);
    }

    @Test
    public void blankInitTest() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new Slot("", null),
                "Slot name can't be blank!"
        );
    }

    @Test
    public void getIdTest() {
        assertNotNull(slot.getId());
        assertFalse(slot.getId().isBlank());
    }

    @Test
    public void getNameTest() {
        assertEquals("Slot", slot.getName());
    }

    @Test
    public void setNameTest() {
        assertThrows(
                IllegalArgumentException.class,
                () -> slot.setName(""),
                "Slot name can't be blank!"
        );
        slot.setName("Test");
        assertEquals("Test", slot.getName());
    }

    @Test
    public void getParentTest() {
        assertEquals(page, slot.getParent());
    }

    private final MockObserver mockObserver = new MockObserver();

    @Test
    public void addObserverTest() {
        slot.addObserver(mockObserver);
        assertEquals(List.of(mockObserver), slot.getObservers());
        assertThrows(
                IllegalArgumentException.class,
                () -> slot.addObserver(null),
                "Observer can't be null!"
        );
        assertThrows(
                IllegalArgumentException.class,
                () -> {
                    slot.addObserver(mockObserver);
                    slot.addObserver(mockObserver);
                },
                String.format("Observer %s is already present!", mockObserver)
        );
    }

    @Test
    public void notifyAllObserversTest() {
        slot.addObserver(mockObserver);
        slot.notifyAllObservers();
        assertEquals(String.format("MockObserver-%s", slot), consoleStream.toString().trim());
    }

    private final MockVisitor mockVisitor = new MockVisitor();

    @Test
    public void acceptVisitorTest() {
        slot.acceptVisitor(mockVisitor);
        assertEquals(String.format("Visited %s", slot), consoleStream.toString().trim());
    }

    @Test
    public void equalsTest() {
        assertEquals(slot, slot);
        assertNotEquals(slot, new Slot("Slot", page));
    }

}
