package rudok.observer;

import rudok.model.Model;

public class MockObserver implements Observer {

    @Override
    public void update(Model value) {
        System.out.println(this + "-" + value);
    }

    @Override
    public String toString() {
        return "MockObserver";
    }

}
