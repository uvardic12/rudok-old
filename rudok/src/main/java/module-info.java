open module rudok {
    requires ioc;
    requires javafx.graphics;
    requires javafx.controls;

    exports rudok;
}