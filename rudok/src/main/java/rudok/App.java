package rudok;

import ioc.engine.IOCEngine;
import ioc.engine.annotation.Component;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import rudok.ui.component.MainComponent;

public class App extends Application {

    public static final double SCREEN_WIDTH = Screen.getPrimary().getBounds().getWidth();

    public static final double SCREEN_HEIGHT = Screen.getPrimary().getBounds().getHeight();

    public static final double MIN_WIDTH = 200;

    public static final double MIN_HEIGHT = 200;

    public static final double MAX_WIDTH = SCREEN_WIDTH;

    public static final double MAX_HEIGHT = SCREEN_HEIGHT;

    @Override
    public void start(Stage stage) {
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
        StageInitializer stageInitializer = IOCEngine.get(StageInitializer.class);
        stageInitializer.initialize(stage);
    }

    @Component
    private static class StageInitializer {

        private final MainComponent mainComponent;

        public StageInitializer(MainComponent mainComponent) {
            this.mainComponent = mainComponent;
        }

        private void initialize(Stage stage) {
            stage.setMinWidth(MIN_WIDTH);
            stage.setMinHeight(MIN_HEIGHT);
            stage.setMaxWidth(MAX_WIDTH);
            stage.setMaxHeight(MAX_HEIGHT);
            stage.setTitle("RuDok");
            stage.setScene(new Scene(mainComponent));
            stage.setMaximized(true);
            stage.show();
        }

    }

}
