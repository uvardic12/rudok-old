package rudok.context;

import rudok.observer.Observer;
import rudok.ui.component.Component;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.stream;

public class Context {

    private static final Set<Component> componentContext = new HashSet<>();

    public static void loadComponents() {
        ClassLoader.loadClasses(Context.class)
                .stream()
                .filter(Context::isComponent)
                .map(Context::instantiateComponent)
                .forEach(Context::addToComponentContext);

        componentContext.forEach(Component::initialize);
    }

    private static boolean isComponent(Class<?> aClass) {
        return !aClass.isAnnotation() && !aClass.isInterface() && Component.class.isAssignableFrom(aClass);
    }

    private static Component instantiateComponent(Class<?> aClass) {
        try {
            return (Component) getDefaultConstructor(aClass).newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private static Constructor<?> getDefaultConstructor(Class<?> aClass) {
        return stream(aClass.getConstructors())
                .filter(constructor -> constructor.getParameterCount() == 0)
                .peek(constructor -> constructor.setAccessible(true))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(
                        String.format("Default constructor not found for class: %s", aClass.getName())
                ));
    }

    private static void addToComponentContext(Component component) {
        if (component == null)
            throw new IllegalArgumentException("Component can't be null after initialization");
        if (componentContext.contains(component))
            throw new IllegalArgumentException(String.format("Component: %s already initialized", component));
        componentContext.add(component);
    }

    public static <C extends Component> C getComponent(Class<C> componentClass) {
        return componentContext.stream()
                .filter(component -> component.getClass().equals(componentClass))
                .map(componentClass::cast)
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(String.format(
                        "Component with class: %s wasn't found", componentClass
                )));
    }

    private static final Set<Observer> observerContext = new HashSet<>();

    public static void loadObservers() {
        ClassLoader.loadClasses(Context.class)
                .stream()
                .filter(Context::isObserver)
                .map(Context::instantiateObserver)
                .forEach(Context::addToObserverContext);
    }

    private static boolean isObserver(Class<?> aClass) {
        return !aClass.isAnnotation() && !aClass.isInterface() && Observer.class.isAssignableFrom(aClass);
    }

    private static Observer instantiateObserver(Class<?> aClass) {
        try {
            return (Observer) getDefaultConstructor(aClass).newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private static void addToObserverContext(Observer observer) {
        if (observer == null)
            throw new IllegalArgumentException("Observer can't be null after initialization");
        if (observerContext.contains(observer))
            throw new IllegalArgumentException(String.format("Observer: %s already initialized", observer));
        observerContext.add(observer);
    }

    public static <O extends Observer> O getObserver(Class<O> observerClass) {
        return observerContext.stream()
                .filter(observer -> observer.getClass().equals(observerClass))
                .map(observerClass::cast)
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(String.format(
                        "Observer with class: %s wasn't found", observerClass
                )));
    }

}
