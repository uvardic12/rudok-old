package rudok.model;

import rudok.observer.Subject;

import java.io.Serializable;
import java.util.List;

public interface Model extends Serializable, Subject {

    String getId();

    String getName();

    void setName(String name);

    Model getParent();

    void addChild(Model child);

    void removeChild(Model child);

    List<Model> getChildren();

    void acceptVisitor(ModelVisitor visitor);

}
