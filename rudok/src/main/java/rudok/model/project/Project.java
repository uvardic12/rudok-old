package rudok.model.project;

import rudok.model.IdGenerator;
import rudok.model.Model;
import rudok.model.ModelVisitor;
import rudok.observer.Observer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Project implements Model {

    private final String id;

    private String name;

    private final Model parent;

    private final List<Model> children = new ArrayList<>();

    private final List<Observer> observers = new ArrayList<>();

    public Project(String name, Model parent) {
        if (name == null || name.isBlank())
            throw new IllegalArgumentException("Name can't be blank!");
        this.id = IdGenerator.generateId();
        this.name = name;
        this.parent = parent;
        this.parent.addChild(this);
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        if (name == null || name.isBlank())
            throw new IllegalArgumentException("Name can't be blank!");
        this.name = name;
        notifyAllObservers();
    }

    @Override
    public void addChild(Model child) {
        if (child == null)
            throw new IllegalArgumentException("Child can't be null!");
        if (children.contains(child))
            throw new IllegalArgumentException(String.format("Child %s is already present!", child));
        children.add(child);
        notifyAllObservers();
    }

    @Override
    public void removeChild(Model child) {
        children.remove(child);
        notifyAllObservers();
    }

    @Override
    public List<Model> getChildren() {
        return List.copyOf(children);
    }

    @Override
    public Model getParent() {
        return parent;
    }

    @Override
    public void addObserver(Observer observer) {
        if (observer == null)
            throw new IllegalArgumentException("Observer can't be null!");
        if (observers.contains(observer))
            throw new IllegalArgumentException(String.format("Observer %s is already present!", observer));
        observers.add(observer);
    }

    @Override
    public void notifyAllObservers() {
        observers.forEach(observer -> observer.update(this));
    }

    List<Observer> getObservers() {
        return List.copyOf(observers);
    }

    @Override
    public void acceptVisitor(ModelVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        return "Project{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", parent=" + parent.getId() +
                ", children=" + children +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Project other)
            return Objects.equals(this.id, other.id);
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
