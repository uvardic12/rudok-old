package rudok.model.slot;

import rudok.model.IdGenerator;
import rudok.model.Model;
import rudok.model.ModelVisitor;
import rudok.observer.Observer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Slot implements Model {

    private final String id;

    private String name;

    private final Model parent;

    private final List<Observer> observers = new ArrayList<>();

    public Slot(String name, Model parent) {
        if (name == null || name.isBlank())
            throw new IllegalArgumentException("Name can't be blank!");
        this.id = IdGenerator.generateId();
        this.name = name;
        this.parent = parent;
        this.parent.addChild(this);
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        if (name == null || name.isBlank())
            throw new IllegalArgumentException("Name can't be blank!");
        this.name = name;
        notifyAllObservers();
    }

    @Override
    public Model getParent() {
        return parent;
    }

    @Override
    public void addChild(Model child) {
        throw new UnsupportedOperationException("Slot can't have children!");
    }

    @Override
    public void removeChild(Model child) {
        throw new UnsupportedOperationException("Slot can't have children!");
    }

    @Override
    public List<Model> getChildren() {
        return List.of();
    }

    @Override
    public void addObserver(Observer observer) {
        if (observer == null)
            throw new IllegalArgumentException("Observer can't be null!");
        if (observers.contains(observer))
            throw new IllegalArgumentException(String.format("Observer %s is already present!", observer));
        observers.add(observer);
    }

    @Override
    public void notifyAllObservers() {
        observers.forEach(observer -> observer.update(this));
    }

    List<Observer> getObservers() {
        return List.copyOf(observers);
    }

    @Override
    public void acceptVisitor(ModelVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        return "Slot{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", parent=" + parent.getId() +
                ", observers=" + observers +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Slot other)
            return Objects.equals(this.id, other.id);
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
