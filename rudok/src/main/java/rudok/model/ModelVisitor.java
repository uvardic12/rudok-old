package rudok.model;

import rudok.model.document.Document;
import rudok.model.page.Page;
import rudok.model.project.Project;
import rudok.model.slot.Slot;
import rudok.model.workspace.Workspace;

public interface ModelVisitor {

    void visit(Workspace workspace);

    void visit(Project project);

    void visit(Document document);

    void visit(Page page);

    void visit(Slot slot);

}
