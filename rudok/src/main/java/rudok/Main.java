package rudok;

import ioc.engine.IOCEngine;
import javafx.application.Application;
import javafx.application.Platform;

public class Main {

    public static void main(String[] args) {
        try {
            Platform.startup(() -> IOCEngine.start(Main.class));
            Application.launch(App.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
