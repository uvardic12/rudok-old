package rudok.ui.dialog;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;

import java.util.Optional;

public class Dialog {

    public enum Type {
        INFORMATION, WARNING, ERROR, CONFIRMATION, TEXT_INPUT
    }

    private final Type type;

    private final String title;

    private final String headerText;

    private final String contentText;

    private Dialog(Builder builder) {
        this.type = builder.type;
        this.title = builder.title;
        this.headerText = builder.headerText;
        this.contentText = builder.contentText;
    }

    public Optional<String> showAndWait() {
        return switch (type) {
            case INFORMATION -> showInformationDialog();
            case WARNING -> showWarningDialog();
            case ERROR -> showErrorDialog();
            case CONFIRMATION -> showConfirmationDialog();
            case TEXT_INPUT -> showTextInputDialog();
        };
    }

    private Optional<String> showInformationDialog() {
        Alert dialog = new Alert(Alert.AlertType.INFORMATION);
        dialog.setTitle(title);
        dialog.setHeaderText(headerText);
        dialog.setContentText(contentText);
        return dialog.showAndWait().map(this::mapButtonType);
    }

    private Optional<String> showWarningDialog() {
        Alert dialog = new Alert(Alert.AlertType.WARNING);
        dialog.setTitle(title);
        dialog.setHeaderText(headerText);
        dialog.setContentText(contentText);
        return dialog.showAndWait().map(this::mapButtonType);
    }

    private Optional<String> showErrorDialog() {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(title);
        dialog.setHeaderText(headerText);
        dialog.setContentText(contentText);
        return dialog.showAndWait().map(this::mapButtonType);
    }

    private Optional<String> showConfirmationDialog() {
        Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
        dialog.setTitle(title);
        dialog.setHeaderText(headerText);
        dialog.setContentText(contentText);
        return dialog.showAndWait().map(this::mapButtonType);
    }

    private Optional<String> showTextInputDialog() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle(title);
        dialog.setHeaderText(headerText);
        dialog.setContentText(contentText);
        return dialog.showAndWait();
    }

    private String mapButtonType(ButtonType buttonType) {
        return switch (buttonType.getButtonData()) {
            case NO, CANCEL_CLOSE -> "N";
            case YES, APPLY, OK_DONE -> "Y";
            default -> throw new IllegalStateException(String.format("Button type: %s not supported!", buttonType));
        };
    }

    public static class Builder {

        private final Type type;

        private String title;

        private String headerText;

        private String contentText;

        public Builder(Type type) {
            this.type = type;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder headerText(String headerText) {
            this.headerText = headerText;
            return this;
        }

        public Builder contentText(String contentText) {
            this.contentText = contentText;
            return this;
        }

        public Dialog build() {
            return new Dialog(this);
        }

    }

}
