package rudok.ui.action.page;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import rudok.context.Context;
import rudok.model.Model;
import rudok.model.page.Page;
import rudok.ui.component.tree.TreeComponent;
import rudok.ui.dialog.Dialog;

public class RemovePageAction implements EventHandler<ActionEvent> {

    @Override
    public void handle(ActionEvent actionEvent) {
        TreeComponent tree = Context.getComponent(TreeComponent.class);
        Model selectedModel = tree.getSelectedItem().getValue().getModel();
        if (!(selectedModel instanceof Page))
            throw new IllegalStateException("Select a page first!");

        new Dialog.Builder(Dialog.Type.CONFIRMATION)
                .title("Remove Page")
                .contentText(String.format("Are you sure you want to remove page: %s?", selectedModel.getName()))
                .build()
                .showAndWait()
                .filter(response -> response.equals("Y"))
                .ifPresent(response -> selectedModel.getParent().removeChild(selectedModel));
    }

}
