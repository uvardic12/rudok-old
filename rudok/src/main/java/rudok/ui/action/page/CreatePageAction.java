package rudok.ui.action.page;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import rudok.context.Context;
import rudok.model.Model;
import rudok.model.document.Document;
import rudok.model.page.Page;
import rudok.ui.component.tree.TreeComponent;
import rudok.ui.component.tree.TreeObserver;
import rudok.ui.dialog.Dialog;

public class CreatePageAction implements EventHandler<ActionEvent> {

    @Override
    public void handle(ActionEvent actionEvent) {
        new Dialog.Builder(Dialog.Type.TEXT_INPUT)
                .title("Create Page")
                .contentText("Name")
                .build()
                .showAndWait()
                .ifPresent(this::createPage);
    }

    private void createPage(String name) {
        TreeComponent tree = Context.getComponent(TreeComponent.class);
        Model selectedModel = tree.getSelectedItem().getValue().getModel();
        if (!(selectedModel instanceof Document))
            throw new IllegalStateException("Select a document first!");
        Page document = new Page(name, selectedModel);
        document.addObserver(Context.getObserver(TreeObserver.class));
    }

}
