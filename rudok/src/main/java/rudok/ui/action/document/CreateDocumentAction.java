package rudok.ui.action.document;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import rudok.context.Context;
import rudok.model.Model;
import rudok.model.document.Document;
import rudok.model.project.Project;
import rudok.ui.component.tree.TreeComponent;
import rudok.ui.component.tree.TreeObserver;
import rudok.ui.dialog.Dialog;

public class CreateDocumentAction implements EventHandler<ActionEvent> {

    @Override
    public void handle(ActionEvent actionEvent) {
        new Dialog.Builder(Dialog.Type.TEXT_INPUT)
                .title("Create Document")
                .contentText("Name")
                .build()
                .showAndWait()
                .ifPresent(this::createDocument);
    }

    private void createDocument(String name) {
        TreeComponent tree = Context.getComponent(TreeComponent.class);
        Model selectedModel = tree.getSelectedItem().getValue().getModel();
        if (!(selectedModel instanceof Project))
            throw new IllegalStateException("Select a project first!");
        Document document = new Document(name, selectedModel);
        document.addObserver(Context.getObserver(TreeObserver.class));
    }

}
