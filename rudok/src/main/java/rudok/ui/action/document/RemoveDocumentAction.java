package rudok.ui.action.document;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import rudok.context.Context;
import rudok.model.Model;
import rudok.model.document.Document;
import rudok.ui.component.tree.TreeComponent;
import rudok.ui.dialog.Dialog;

public class RemoveDocumentAction implements EventHandler<ActionEvent> {

    @Override
    public void handle(ActionEvent actionEvent) {
        TreeComponent tree = Context.getComponent(TreeComponent.class);
        Model selectedModel = tree.getSelectedItem().getValue().getModel();
        if (!(selectedModel instanceof Document))
            throw new IllegalStateException("Select a document first!");

        new Dialog.Builder(Dialog.Type.CONFIRMATION)
                .title("Remove Document")
                .contentText(String.format("Are you sure you want to remove document: %s?", selectedModel.getName()))
                .build()
                .showAndWait()
                .filter(response -> response.equals("Y"))
                .ifPresent(response -> selectedModel.getParent().removeChild(selectedModel));
    }

}
