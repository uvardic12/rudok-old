package rudok.ui.action.project;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import rudok.context.Context;
import rudok.model.project.Project;
import rudok.model.workspace.Workspace;
import rudok.ui.component.pane.WorkspacePaneObserver;
import rudok.ui.component.tree.TreeObserver;
import rudok.ui.dialog.Dialog;

public class CreateProjectAction implements EventHandler<ActionEvent> {

    @Override
    public void handle(ActionEvent actionEvent) {
        new Dialog.Builder(Dialog.Type.TEXT_INPUT)
                .title("Create Project")
                .contentText("Name")
                .build()
                .showAndWait()
                .ifPresent(this::createProject);
    }

    private void createProject(String name) {
        Project project = new Project(name, Workspace.getInstance());
        project.addObserver(Context.getObserver(TreeObserver.class));
        project.addObserver(Context.getObserver(WorkspacePaneObserver.class));
    }

}
