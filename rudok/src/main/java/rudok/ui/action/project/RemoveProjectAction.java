package rudok.ui.action.project;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import rudok.context.Context;
import rudok.model.Model;
import rudok.model.project.Project;
import rudok.ui.component.tree.TreeComponent;
import rudok.ui.dialog.Dialog;

public class RemoveProjectAction implements EventHandler<ActionEvent> {

    @Override
    public void handle(ActionEvent actionEvent) {
        TreeComponent tree = Context.getComponent(TreeComponent.class);
        Model selectedModel = tree.getSelectedItem().getValue().getModel();
        if (!(selectedModel instanceof Project))
            throw new IllegalStateException("Select a project first!");

        new Dialog.Builder(Dialog.Type.CONFIRMATION)
                .title("Remove Project")
                .contentText(String.format("Are you sure you want to remove project: %s?", selectedModel.getName()))
                .build()
                .showAndWait()
                .filter(response -> response.equals("Y"))
                .ifPresent(response -> selectedModel.getParent().removeChild(selectedModel));
    }

}
