package rudok.ui.action.project;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import rudok.context.Context;
import rudok.model.Model;
import rudok.model.project.Project;
import rudok.ui.component.tree.TreeComponent;
import rudok.ui.dialog.Dialog;

public class RenameProjectAction implements EventHandler<ActionEvent> {

    @Override
    public void handle(ActionEvent actionEvent) {
        TreeComponent tree = Context.getComponent(TreeComponent.class);
        Model selectedModel = tree.getSelectedItem().getValue().getModel();
        if (!(selectedModel instanceof Project))
            throw new IllegalStateException("Select a project first!");

        new Dialog.Builder(Dialog.Type.TEXT_INPUT)
                .title("Rename Project")
                .contentText("New name")
                .build()
                .showAndWait()
                .ifPresent(selectedModel::setName);
    }

}
