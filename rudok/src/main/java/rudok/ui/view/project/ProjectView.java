package rudok.ui.view.project;

import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import rudok.model.Model;
import rudok.model.document.Document;
import rudok.model.project.Project;
import rudok.ui.view.DocumentView;
import rudok.ui.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProjectView extends TabPane implements View {

    private final Project project;

    private final List<DocumentView> documentViews = new ArrayList<>();

    public ProjectView(Project project) {
        this.project = project;
        getTabs().add(new Tab(project.getName()));
    }

    @Override
    public Model getModel() {
        return project;
    }

//    @Override
//    public void update(Model value) {
//        if (!(value instanceof Project project))
//            throw new IllegalStateException(String.format("Illegal update on Project view with model: %s", value));
//
//        project.getChildren().forEach(this::addDocumentView);
//    }

    private void addDocumentView(Model model) {
        if (!(model instanceof Document document))
            throw new IllegalStateException(String.format("Illegal model: %s for document view!", model));

        if (!isDocumentPresent(document)) {
            DocumentView documentView = new DocumentView(document);
            getTabs().add(documentView);
            documentViews.add(documentView);
        }
    }

    private boolean isDocumentPresent(Document document) {
        return documentViews.stream()
                .anyMatch(documentView -> documentView.getModel().equals(document));
    }

    public void selectTab(Document document) {
        DocumentView documentView = findDocumentView(document);
        getSelectionModel().select(documentView);
    }

    private DocumentView findDocumentView(Document document) {
        return documentViews.stream()
                .filter(dv -> dv.getModel().equals(document))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Document: %s not found!", document)));
    }

    @Override
    public String toString() {
        return "ProjectView{" +
                "project=" + project +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ProjectView other)
            return Objects.equals(this.project, other.project);
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(project);
    }
}
