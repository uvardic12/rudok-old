package rudok.ui.view.tree;

import rudok.model.Model;
import rudok.ui.view.View;

import java.util.Objects;

public class TreeItemView implements View {

    private final Model model;

    public TreeItemView(Model model) {
        this.model = model;
    }

    @Override
    public Model getModel() {
        return model;
    }

    @Override
    public String toString() {
        return model.getName();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof TreeItemView other)
            return Objects.equals(this.model, other.model);
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(model);
    }

}
