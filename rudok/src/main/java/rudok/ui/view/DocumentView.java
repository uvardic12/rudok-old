package rudok.ui.view;

import javafx.scene.control.Tab;
import rudok.model.Model;
import rudok.model.document.Document;

import java.util.Objects;

public class DocumentView extends Tab implements View {

    private final Document document;

    public DocumentView(Document document) {
        this.document = document;
        setClosable(false);
        setText(formatName());
    }

    private String formatName() {
        return String.format("%s - %s", document.getParent().getName(), document.getName());
    }

    @Override
    public Model getModel() {
        return document;
    }

    @Override
    public String toString() {
        return document.getName();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof DocumentView other)
            return Objects.equals(this.document, other.document);
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(document);
    }

}
