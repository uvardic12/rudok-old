package rudok.ui.view;

import rudok.model.Model;

public interface View {

    Model getModel();

}
