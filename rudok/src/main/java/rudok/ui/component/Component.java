package rudok.ui.component;

public interface Component {

    void initialize();

}
