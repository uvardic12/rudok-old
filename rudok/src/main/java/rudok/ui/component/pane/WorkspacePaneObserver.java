package rudok.ui.component.pane;

import rudok.context.Context;
import rudok.model.Model;
import rudok.model.project.Project;
import rudok.model.workspace.Workspace;
import rudok.observer.Observer;
import rudok.ui.view.project.ProjectView;

public class WorkspacePaneObserver implements Observer {

    @Override
    public void update(Model value) {
        if (value instanceof Workspace workspace)
            workspace.getChildren().forEach(this::updateView);
        else if (value instanceof Project project)
            updateView(project);
        else
            throw new IllegalStateException(
                    String.format("Illegal update on Workspace Pane Component with model: %s", value)
            );
    }

    private void updateView(Model model) {
        if (!(model instanceof Project project))
            throw new IllegalStateException(String.format("Illegal model: %s for project view!", model));

        WorkspacePaneComponent workspacePaneComponent = Context.getComponent(WorkspacePaneComponent.class);

        workspacePaneComponent.findProjectView(project).ifPresent(workspacePaneComponent::removeProjectView);

        ProjectView newView = new ProjectView(project);
        workspacePaneComponent.addProjectView(newView);
        workspacePaneComponent.getChildren().clear();
        workspacePaneComponent.getChildren().add(newView);
    }

}
