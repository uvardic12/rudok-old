package rudok.ui.component.pane;

import javafx.scene.layout.Pane;
import rudok.context.Context;
import rudok.model.project.Project;
import rudok.model.workspace.Workspace;
import rudok.ui.component.Component;
import rudok.ui.view.project.ProjectView;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class WorkspacePaneComponent extends Pane implements Component {

    private final List<ProjectView> projectViews = new ArrayList<>();

    public WorkspacePaneComponent() {
        Workspace.getInstance().addObserver(Context.getObserver(WorkspacePaneObserver.class));
    }

    @Override
    public void initialize() {}

    public void addProjectView(ProjectView view) {
        if (view == null)
            throw new IllegalArgumentException("Project view can't be null!");
        if (projectViews.contains(view))
            throw new IllegalArgumentException(String.format("Project view: %s is already present!", projectViews));
        projectViews.add(view);
    }

    public void removeProjectView(ProjectView view) {
        projectViews.remove(view);
    }

    public Optional<ProjectView> findProjectView(Project project) {
        return projectViews.stream()
                .filter(pv -> pv.getModel().equals(project))
                .findFirst();
    }

    public ProjectView findProjectViewOrThrow(Project project) {
        return findProjectView(project)
                .orElseThrow(() -> new IllegalArgumentException(String.format("Project: %s not found!", project)));
    }

    public void setContent(Project project) {
        ProjectView projectView = findProjectViewOrThrow(project);
        getChildren().clear();
        getChildren().add(projectView);
    }

}
