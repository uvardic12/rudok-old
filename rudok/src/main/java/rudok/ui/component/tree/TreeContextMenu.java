package rudok.ui.component.tree;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import rudok.model.ModelVisitor;
import rudok.model.document.Document;
import rudok.model.page.Page;
import rudok.model.project.Project;
import rudok.model.slot.Slot;
import rudok.model.workspace.Workspace;
import rudok.ui.action.document.CreateDocumentAction;
import rudok.ui.action.document.RemoveDocumentAction;
import rudok.ui.action.document.RenameDocumentAction;
import rudok.ui.action.page.CreatePageAction;
import rudok.ui.action.page.RemovePageAction;
import rudok.ui.action.page.RenamePageAction;
import rudok.ui.action.project.CreateProjectAction;
import rudok.ui.action.project.RemoveProjectAction;
import rudok.ui.action.project.RenameProjectAction;

public class TreeContextMenu extends ContextMenu implements ModelVisitor {

    @Override
    public void visit(Workspace workspace) {
        getItems().clear();

        MenuItem createProjectMenuItem = new MenuItem("Create Project");
        createProjectMenuItem.setOnAction(new CreateProjectAction());
        getItems().add(createProjectMenuItem);
    }

    @Override
    public void visit(Project project) {
        getItems().clear();

        MenuItem createDocumentMenuItem = new MenuItem("Create Document");
        createDocumentMenuItem.setOnAction(new CreateDocumentAction());
        getItems().add(createDocumentMenuItem);

        getItems().add(new SeparatorMenuItem());

        MenuItem renameProjectMenuItem = new MenuItem("Rename Project");
        renameProjectMenuItem.setOnAction(new RenameProjectAction());
        getItems().add(renameProjectMenuItem);

        MenuItem removeProjectMenuItem = new MenuItem("Remove Project");
        removeProjectMenuItem.setOnAction(new RemoveProjectAction());
        getItems().add(removeProjectMenuItem);
    }

    @Override
    public void visit(Document document) {
        getItems().clear();

        MenuItem createPageMenuItem = new MenuItem("Create Page");
        createPageMenuItem.setOnAction(new CreatePageAction());
        getItems().add(createPageMenuItem);

        getItems().add(new SeparatorMenuItem());

        MenuItem renameDocumentMenuItem = new MenuItem("Rename Document");
        renameDocumentMenuItem.setOnAction(new RenameDocumentAction());
        getItems().add(renameDocumentMenuItem);

        MenuItem removeDocumentMenuItem = new MenuItem("Remove Document");
        removeDocumentMenuItem.setOnAction(new RemoveDocumentAction());
        getItems().add(removeDocumentMenuItem);
    }

    @Override
    public void visit(Page page) {
        getItems().clear();

        MenuItem renamePageMenuItem = new MenuItem("Rename Page");
        renamePageMenuItem.setOnAction(new RenamePageAction());
        getItems().add(renamePageMenuItem);

        MenuItem removePageMenuItem = new MenuItem("Remove Page");
        removePageMenuItem.setOnAction(new RemovePageAction());
        getItems().add(removePageMenuItem);
    }

    @Override
    public void visit(Slot slot) {
        getItems().clear();
    }

}
