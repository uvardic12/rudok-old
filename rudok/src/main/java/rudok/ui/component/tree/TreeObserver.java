package rudok.ui.component.tree;

import javafx.scene.control.TreeItem;
import rudok.context.Context;
import rudok.model.Model;
import rudok.model.workspace.Workspace;
import rudok.observer.Observer;
import rudok.ui.view.tree.TreeItemView;

public class TreeObserver implements Observer {

    @Override
    public void update(Model value) {
        TreeItem<TreeItemView> oldItem = findTreeItem(value);
        TreeItem<TreeItemView> newItem = new TreeItem<>(new TreeItemView(value));

        initializeChildItems(newItem);
        newItem.setExpanded(true);

        if (value.equals(Workspace.getInstance()))
            Context.getComponent(TreeComponent.class).setRoot(newItem);
        else {
            int oldItemIndex = oldItem.getParent().getChildren().indexOf(oldItem);
            oldItem.getParent().getChildren().set(oldItemIndex, newItem);
        }
    }

    private TreeItem<TreeItemView> foundItem;

    private TreeItem<TreeItemView> findTreeItem(Model model) {
        TreeItem<TreeItemView> root = Context.getComponent(TreeComponent.class).getRoot();
        findTreeItemWorker(model, root);
        if (foundItem == null)
            throw new IllegalArgumentException(String.format("Model %s not found!", model));
        return foundItem;
    }

    private void findTreeItemWorker(Model model, TreeItem<TreeItemView> root) {
        if (root.getValue().getModel().equals(model)) {
            foundItem = root;
            return;
        }

        for (TreeItem<TreeItemView> item : root.getChildren()) {
            if (item.getValue().getModel().equals(model))
                foundItem = item;
            findTreeItemWorker(model, item);
        }
    }

    private void initializeChildItems(TreeItem<TreeItemView> root) {
        root.setExpanded(true);
        for (Model child : root.getValue().getModel().getChildren()) {
            TreeItem<TreeItemView> item = new TreeItem<>(new TreeItemView(child));
            item.setExpanded(true);
            root.getChildren().add(item);
            initializeChildItems(item);
        }
    }

}
