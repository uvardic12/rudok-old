package rudok.ui.component.tree;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TreeItem;
import rudok.context.Context;
import rudok.model.Model;
import rudok.model.ModelVisitor;
import rudok.model.document.Document;
import rudok.model.page.Page;
import rudok.model.project.Project;
import rudok.model.slot.Slot;
import rudok.model.workspace.Workspace;
import rudok.ui.component.pane.WorkspacePaneComponent;
import rudok.ui.view.tree.TreeItemView;

public class TreeSelectionListener implements ChangeListener<TreeItem<TreeItemView>>, ModelVisitor {

    @Override
    public void changed(
            ObservableValue<? extends TreeItem<TreeItemView>> observable,
            TreeItem<TreeItemView> oldValue,
            TreeItem<TreeItemView> newValue
    ) {
        if (newValue != null) {
            Model model = newValue.getValue().getModel();
            model.acceptVisitor(this);
        }
    }

    @Override
    public void visit(Workspace workspace) {}

    @Override
    public void visit(Project project) {
        WorkspacePaneComponent workspacePaneComponent = Context.getComponent(WorkspacePaneComponent.class);
        workspacePaneComponent.setContent(project);
    }

    @Override
    public void visit(Document document) {
//        document.getParent().acceptVisitor(this);
//
//        WorkspacePaneComponent workspacePaneComponent = Context.getComponent(WorkspacePaneComponent.class);
//        ProjectView selectedProjectView = workspacePaneComponent.findProjectViewOrThrow((Project) document.getParent());
//        selectedProjectView.selectTab(document);
    }

    @Override
    public void visit(Page page) {

    }

    @Override
    public void visit(Slot slot) {

    }

}
