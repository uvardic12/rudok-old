package rudok.ui.component.tree;

import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import rudok.context.Context;
import rudok.model.workspace.Workspace;
import rudok.ui.component.Component;
import rudok.ui.view.tree.TreeItemView;

public class TreeComponent extends TreeView<TreeItemView> implements Component {

    private final TreeContextMenu treeContextMenu = new TreeContextMenu();

    @Override
    public void initialize() {
        Workspace.getInstance().addObserver(Context.getObserver(TreeObserver.class));
        setRoot(new TreeItem<>(new TreeItemView(Workspace.getInstance())));
        setContextMenu(treeContextMenu);
        addEventHandler(MouseEvent.MOUSE_CLICKED, new TreeMouseEventHandler());
        getSelectionModel().selectedItemProperty().addListener(new TreeSelectionListener());
    }

    public TreeItem<TreeItemView> getSelectedItem() {
        TreeItem<TreeItemView> selectedItem = getSelectionModel().getSelectedItem();
        if (selectedItem == null)
            throw new IllegalStateException("No item is selected!");
        return selectedItem;
    }

    public TreeContextMenu getTreeContextMenu() {
        return treeContextMenu;
    }

}
