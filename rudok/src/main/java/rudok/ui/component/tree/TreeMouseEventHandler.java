package rudok.ui.component.tree;

import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import rudok.context.Context;
import rudok.model.Model;

public class TreeMouseEventHandler implements EventHandler<MouseEvent> {

    @Override
    public void handle(MouseEvent mouseEvent) {
        if (mouseEvent.getButton().equals(MouseButton.SECONDARY) && mouseEvent.getClickCount() == 1)
            initializeTreeContextMenu();
    }

    private void initializeTreeContextMenu() {
        TreeComponent tree = Context.getComponent(TreeComponent.class);
        try {
            Model selectedModel = tree.getSelectedItem().getValue().getModel();
            selectedModel.acceptVisitor(tree.getTreeContextMenu());
        // ignore if nothing is selected, we don't display the ioc.context menu
        } catch (IllegalStateException ignored) {}
    }

}
