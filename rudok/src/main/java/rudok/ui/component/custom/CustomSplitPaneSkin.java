package rudok.ui.component.custom;

import javafx.scene.control.SplitPane;
import javafx.scene.control.skin.SplitPaneSkin;

public class CustomSplitPaneSkin extends SplitPaneSkin {

    public CustomSplitPaneSkin(SplitPane control) {
        super(control);
    }

    @Override
    protected void layoutChildren(double x, double y, double w, double h) {
        double[] dividerPositions = getSkinnable().getDividerPositions();
        super.layoutChildren(x, y, w, h);
        getSkinnable().setDividerPositions(dividerPositions);
    }

}
