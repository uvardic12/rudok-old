package rudok.ui.component;

import ioc.engine.annotation.Component;
import ioc.engine.annotation.Initialize;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import rudok.context.Context;
import rudok.ui.component.custom.CustomSplitPaneSkin;
import rudok.ui.component.pane.WorkspacePaneComponent;
import rudok.ui.component.tree.TreeComponent;

@Component
public class MainComponent extends VBox {

    @Initialize
    public void initialize() {
        System.out.println("Initialize: " + this);
    }

    private SplitPane initializeSplitPane() {
        SplitPane splitPane = new SplitPane();
        splitPane.setSkin(new CustomSplitPaneSkin(splitPane));
        splitPane.getItems().add(Context.getComponent(TreeComponent.class));
        splitPane.getItems().add(Context.getComponent(WorkspacePaneComponent.class));
        splitPane.setDividerPosition(0, 0.25);
        setVgrow(splitPane, Priority.ALWAYS);
        return splitPane;
    }

}
