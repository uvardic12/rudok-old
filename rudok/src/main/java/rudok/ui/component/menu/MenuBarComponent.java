package rudok.ui.component.menu;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import rudok.ui.action.document.CreateDocumentAction;
import rudok.ui.action.document.RemoveDocumentAction;
import rudok.ui.action.document.RenameDocumentAction;
import rudok.ui.action.page.CreatePageAction;
import rudok.ui.action.page.RemovePageAction;
import rudok.ui.action.page.RenamePageAction;
import rudok.ui.action.project.CreateProjectAction;
import rudok.ui.action.project.RemoveProjectAction;
import rudok.ui.action.project.RenameProjectAction;
import rudok.ui.component.Component;


public class MenuBarComponent extends MenuBar implements Component {

    @Override
    public void initialize() {
        initializeFileMenu();
    }

    private void initializeFileMenu() {
        Menu fileMenu = new Menu("File");

        fileMenu.getItems().add(initializeProjectFileMenu());
        fileMenu.getItems().add(initializeDocumentFileMenu());
        fileMenu.getItems().add(initializePageFileMenu());

        getMenus().add(fileMenu);
    }

    private Menu initializeProjectFileMenu() {
        Menu projectMenu = new Menu("Project");

        MenuItem createProjectMenuItem = new MenuItem("Create Project");
        createProjectMenuItem.setOnAction(new CreateProjectAction());
        projectMenu.getItems().add(createProjectMenuItem);

        MenuItem renameProjectMenuItem = new MenuItem("Rename Project");
        renameProjectMenuItem.setOnAction(new RenameProjectAction());
        projectMenu.getItems().add(renameProjectMenuItem);

        MenuItem removeProjectMenuItem = new MenuItem("Remove Project");
        removeProjectMenuItem.setOnAction(new RemoveProjectAction());
        projectMenu.getItems().add(removeProjectMenuItem);

        return projectMenu;
    }

    private Menu initializeDocumentFileMenu() {
        Menu documentMenu = new Menu("Document");

        MenuItem createDocumentMenuItem = new MenuItem("Create Document");
        createDocumentMenuItem.setOnAction(new CreateDocumentAction());
        documentMenu.getItems().add(createDocumentMenuItem);

        MenuItem renameDocumentMenuItem = new MenuItem("Rename Document");
        renameDocumentMenuItem.setOnAction(new RenameDocumentAction());
        documentMenu.getItems().add(renameDocumentMenuItem);

        MenuItem removeDocumentMenuItem = new MenuItem("Remove Document");
        removeDocumentMenuItem.setOnAction(new RemoveDocumentAction());
        documentMenu.getItems().add(removeDocumentMenuItem);

        return documentMenu;
    }

    private Menu initializePageFileMenu() {
        Menu pageMenu = new Menu("Page");

        MenuItem createPageMenuItem = new MenuItem("Create Page");
        createPageMenuItem.setOnAction(new CreatePageAction());
        pageMenu.getItems().add(createPageMenuItem);

        MenuItem renamePageMenuItem = new MenuItem("Rename Page");
        renamePageMenuItem.setOnAction(new RenamePageAction());
        pageMenu.getItems().add(renamePageMenuItem);

        MenuItem removePageMenuItem = new MenuItem("Remove Page");
        removePageMenuItem.setOnAction(new RemovePageAction());
        pageMenu.getItems().add(removePageMenuItem);

        return pageMenu;
    }

}
