package rudok;

import rudok.ui.dialog.Dialog;

public class ExceptionHandler implements Thread.UncaughtExceptionHandler {

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        new Dialog.Builder(Dialog.Type.ERROR)
                .title(formatExceptionName(ex))
                .contentText(ex.getMessage())
                .build()
                .showAndWait();
    }

    private String formatExceptionName(Throwable ex) {
        String name = ex.getClass().getSimpleName().replace("Exception", "");
        String[] words = name.split("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])");
        return String.join(" ", words);
    }

}
