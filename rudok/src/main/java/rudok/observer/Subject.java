package rudok.observer;

public interface Subject {

    void addObserver(Observer observer);

    void notifyAllObservers();

}
