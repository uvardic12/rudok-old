package rudok.observer;

import rudok.model.Model;

public interface Observer {

    void update(Model value);

}
